#include <sys/types.h>
#include <sys/stat.h>

#include <glib.h>

#include <errno.h>
#include <unistd.h>

#include "prefs.h"

// somewhere to store our settings
GHashTable *settings;

// standard key names for prefs
const char *PREFS_FILE = "config_file";
const char *PREFS_TIME_FORMAT = "time_format";
const char *PREFS_TIME_STYLE = "time_style";
const char *PREFS_DATE_FORMAT = "date_format";
const char *PREFS_DATE_STYLE = "date_style";
const char *PREFS_PAD_LEFT = "pad_left";
const char *PREFS_PAD_RIGHT = "pad_right";
const char *PREFS_TZ_LIST = "tz_list";
const char *PREFS_TZ_LABEL_STYLE = "tz_label_style";
const char *PREFS_TZ_TIME_FORMAT = "tz_format";
const char *PREFS_TZ_TIME_STYLE = "tz_style";

// fetch a preference for a given key
const void *pref_for_key(const char *const key) {

	return g_hash_table_lookup(settings, key);
}

// set a string preference - handles copying the value to a new memory location
void set_string_pref_for_key(const char *const key, const char *const value) {
	gpointer copy;

	if ((copy = g_strdup(value)) == NULL) {
		g_warning("g_strdup failed: %s", g_strerror(errno));
		return;
	}

	g_hash_table_replace(settings, (gpointer)key, copy);
}

// set an int preference - handles copying the value to a new memory location
void set_int_pref_for_key(const char *const key, const int value) {
	gpointer copy;

	if ((copy = g_malloc(sizeof(value))) == NULL) {
		g_warning("g_malloc failed: %s", g_strerror(errno));
		return;
	} else {
		*(int *)copy = value;
	}

	g_hash_table_replace(settings, (gpointer)key, copy);
}

// return false if a file doesn't exist
int file_exists(const char *name) {
	struct stat sb;

	if ((stat(name, &sb) != 0) && (errno == ENOENT)) {
		// doesn't exist
		return 0;
	} else {
		// does, or we can't tell
		return 1;
	}
}

// allocate memory for our settings and set up constant prefs
void initialise_prefs(const char *const user_supplied_path) {
	const char *path;
	
	// somewhere to store our settings
	settings = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, g_free);

	// store the location of our config file
	if (user_supplied_path != NULL) {
		set_string_pref_for_key(PREFS_FILE, user_supplied_path);
		if (! file_exists(user_supplied_path)) {
			// if the user gives a us a file that doesn't exist we should probably say
			g_warning("can't find config file \"%s\": %s", user_supplied_path, g_strerror(errno));
		}
	} else {
		// use the default location - it may or may not have something there
		path = g_strdup_printf("%s/dockclock/main.cfg", g_get_user_config_dir());
		if (path == NULL) {
			g_critical("g_strdup_printf failed: %s", g_strerror(errno));
		} else {
			g_hash_table_insert(settings, (gpointer)PREFS_FILE, (gpointer)path);
		}
	}
}

// retrieve a string from the file parser using the provided key. return 0 or 1
int fetch_string_from_parser(GKeyFile *parser, const char *key, const char **value) {
	GError *gerr = NULL;

	// if there is no parser (perhaps there is no file) then return immediately
	if (parser == NULL) {
		return 0;
	}

	g_clear_error(&gerr);
	*value = g_key_file_get_string(parser, "main", key, &gerr);
	if (gerr != NULL) {
		// we only want to warn about errors - not setting something is fine
		if (! g_error_matches(gerr, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND) &&
				! g_error_matches(gerr, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_KEY_NOT_FOUND)) {
			g_warning("fetch of preference \"%s\" failed: %s", key, gerr->message);
		}
		g_error_free(gerr);
		return 0;
	} else {
		return 1;
	}
}

// retrieve an integer from the file parser using the provided key. return 0 or 1
int fetch_int_from_parser(GKeyFile *parser, const char *key, int *value) {
	GError *gerr = NULL;

	if (parser == NULL) {
		return 0;
	}

	g_clear_error(&gerr);
	*value = g_key_file_get_integer(parser, "main", key, &gerr);
	if (gerr != NULL) {
		if (! g_error_matches(gerr, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND) &&
				! g_error_matches(gerr, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_KEY_NOT_FOUND)) {
			g_warning("fetch of preference \"%s\" failed: %s", key, gerr->message);
		}
		g_error_free(gerr);
		return 0;
	} else {
		return 1;
	}
}

void free_tz_entry(gpointer entry) {

	if (entry != NULL) {
		if (((struct time_zone *)entry)->label != NULL) {
			g_free((gpointer)((struct time_zone *)entry)->label);
		}
		if (((struct time_zone *)entry)->tz != NULL) {
			g_time_zone_unref(((struct time_zone *)entry)->tz);
		}
		g_free(entry);
	}
}

// handle any list of time zones
void read_tz_list(GKeyFile *parser) {
	GError *gerr = NULL;
	GSList *list;
	gchar **key_list;
	gchar *curr_key;
	int i;
	struct time_zone *entry;

	// we have to remove any current setting first being careful to free what needs freeing
	list = g_hash_table_lookup(settings, (gpointer)PREFS_TZ_LIST);
	if (list != NULL) {
		// there is a list to destroy
		g_hash_table_steal(settings, (gpointer)PREFS_TZ_LIST); //remove from hash without g_freeing
		// now clean up the list
		g_slist_free_full(list, free_tz_entry);
	}

	// if there is no file to read then there are no tzs and our job is done 
	if (parser == NULL) {
		return;
	}

	// fetch the list of labels
	g_clear_error(&gerr);
	key_list = g_key_file_get_keys(parser, "time_zones", NULL, &gerr);
	if (gerr != NULL) {
		if (! g_error_matches(gerr, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND)) {
			// something went wrong
			g_warning("fetch of timezones from preference file failed: %s", gerr->message);
		}
		g_clear_error(&gerr);
		return;
	}
	// now we have the labels we need a list to hold an entry for each
	list = NULL;
	// go through each label and fetch the tz
	for (i = 0, curr_key = key_list[i]; curr_key != NULL; curr_key = key_list[++i]) {
		g_debug("looking at %s", curr_key);
		if ((entry = g_malloc(sizeof(*entry))) == NULL) {
			g_warning("g_malloc failed: %s", g_strerror(errno));
		} else {
			// create an entry for this label/tz pair
			// we dup this as we don't know if we can only free the array returned from
			// get_keys if we also free its elements
			entry->label = g_strdup(curr_key);
			entry->tz = g_time_zone_new_identifier(g_key_file_get_string(parser, "time_zones", curr_key, &gerr));
			if (entry->tz == NULL) {
				g_warning("unable to parse TZ \"%s\"", g_key_file_get_string(parser, "time_zones", curr_key, &gerr));
				g_free((void *)(entry->label));
				g_free(entry);
				continue;
			}
			// is quicker to build the list in reverse then reverse it at the end
			list = g_slist_prepend(list, (gpointer)entry);
		}
	}
	// now free the return of get_keys as we have dup'ed each one
	g_strfreev(key_list);
	// and put the list in the order the user entered them
	list = g_slist_reverse(list);

	// add list to prefs
	g_hash_table_insert(settings, (gpointer)PREFS_TZ_LIST, (gpointer)list);
}


// read a config file and update the settings hash. default any preferences not set
void read_config(void) {
	GKeyFile *key_file;
	GError *gerr = NULL;
	const char *value;
	int intval;
	struct prefs_defaults_string string_prefs[] = {
		{ PREFS_TIME_FORMAT, "%R" },
		{ PREFS_TIME_STYLE, "bold 10" },
		{ PREFS_DATE_FORMAT, "%a %d %b" },
		{ PREFS_DATE_STYLE, "8" },
		{ PREFS_TZ_LABEL_STYLE, "bold 10" },
		{ PREFS_TZ_TIME_FORMAT, "%R %Z" },
		{ PREFS_TZ_TIME_STYLE, "10" }
	};
	struct prefs_defaults_int int_prefs[] = {
		{ PREFS_PAD_LEFT, 2 },
		{ PREFS_PAD_RIGHT, 0 }
	};
	int i;
	
	// find the path to the config file
	value = pref_for_key(PREFS_FILE);

	if (file_exists(value)) {
		// create a file parser
		key_file = g_key_file_new();
		// and parse
		g_clear_error(&gerr);
		if (! g_key_file_load_from_file(key_file, value, G_KEY_FILE_NONE, &gerr)) {
			g_warning("parse of config file \"%s\" failed: %s", value, gerr->message);
			g_clear_error(&gerr);
		}
	} else {
		key_file = NULL;
	}
	
	// load any string settings into our hash
	for (i = 0; i < G_N_ELEMENTS(string_prefs); i++) {
		if (fetch_string_from_parser(key_file, string_prefs[i].key, &value)) {
			set_string_pref_for_key(string_prefs[i].key, value);
		} else {
			set_string_pref_for_key(string_prefs[i].key, string_prefs[i].value);
		}
	}
	// now the ints
	for (i = 0; i < G_N_ELEMENTS(int_prefs); i++) {
		if (fetch_int_from_parser(key_file, int_prefs[i].key, &intval)) {
			set_int_pref_for_key(int_prefs[i].key, intval);
		} else {
			set_int_pref_for_key(int_prefs[i].key, int_prefs[i].value);
		}
	}

	// now we get the list of time zones
	read_tz_list(key_file);

	// we may or may not have one but free it if we do
	if (key_file != NULL) {
		g_key_file_free(key_file);
	}
}
