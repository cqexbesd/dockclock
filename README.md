# dockclock

A clock that displays in your dock. Can display times in other time zones if
you click on it and date and time format is configurable but mostly it's just
simple and unobtrusive.
