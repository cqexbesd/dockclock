/* $Id: dockclock.h,v 1.3 2013/02/10 18:22:50 andrew Exp $ */
#ifndef DOCKCLOCK_H
#define DOCKCLOCK_H

#include <gtk/gtk.h>
#include <glib.h>

/* a place to store our GUI elements that need updating periodically */
struct widgets {
	GtkWindow *window;
	GtkFrame *frame;
	GtkLabel *time;
	GtkLabel *date;
};

// update the data shown in the labels
void update_display(struct widgets *widgets);
// a method to be called periodically to do whatever screen updates are needed
gboolean schedule_display_updates(gpointer widgets);

void set_style_on_label(GtkLabel *label, const char *style_string);
void apply_styles_to_gui(struct widgets *widgets);

// helper for create_gui
void create_label(GtkLabel **label);

/* build and show the GUI elements */
void create_gui(struct widgets *widgets);

gboolean sighup_handler(gpointer widgets);

#endif
