# $Id: Makefile,v 1.5 2018/12/27 22:45:17 andrew Exp $
PROG=dockclock
SRCS=dockclock.c prefs.c
MAN=dockclock.1
PKG_CONF_PKGS=glib-2.0 gtk+-3.0
CFLAGS+=`pkg-config --cflags ${PKG_CONF_PKGS}`
LDFLAGS+=`pkg-config --libs-only-other ${PKG_CONF_PKGS}`
LDADD+=`pkg-config --libs-only-L ${PKG_CONF_PKGS}` `pkg-config --libs-only-l ${PKG_CONF_PKGS}` -lX11

.ifdef DEBUG
CFLAGS+=-Wall -g -O0
.else
CFLAGS+=-Os
PREFIX?=/usr/local
BINDIR?=${PREFIX}/bin
MANDIR?=${PREFIX}/man/man
.endif

.include <bsd.prog.mk>
