/* $Id: prefs.h,v 1.3 2013/02/12 18:19:45 andrew Exp $ */
#ifndef PREFS_H
#define PREFS_H

#include <gtk/gtk.h>

// where we store the prefs
extern GHashTable *settings;

// standard key names for prefs
extern const char *PREFS_FILE;
extern const char *PREFS_TIME_FORMAT;
extern const char *PREFS_TIME_STYLE;
extern const char *PREFS_DATE_FORMAT;
extern const char *PREFS_DATE_STYLE;
extern const char *PREFS_PAD_LEFT;
extern const char *PREFS_PAD_RIGHT;
extern const char *PREFS_TZ_LIST;
extern const char *PREFS_TZ_LABEL_STYLE;
extern const char *PREFS_TZ_TIME_FORMAT;
extern const char *PREFS_TZ_TIME_STYLE;

// entry in table of prefs with defaults
struct prefs_defaults_string {
	const char *key;
	const char *value;
};
struct prefs_defaults_int {
	const char *key;
	const int value;
};

// entry in table of tzs
struct time_zone {
	const char *label;
	GTimeZone *tz;
};

// fetch a preference for a given key
const void *pref_for_key(const char *const key);
// set a string preference
void set_string_pref_for_key(const char *const key, const char *const value);
// set an int preference
void set_int_pref_for_key(const char *const key, const int value);
// return false if a file doesn't exist
int file_exists(const char *name);
// do first time around setup
void initialise_prefs(const char *const user_supplied_path);
// retrieve a string from the file parser using the provided key. return 0 or 1
int fetch_string_from_parser(GKeyFile *parser, const char *key, const char **value);
// retrieve an integer from the file parser using the provided key. return 0 or 1
int fetch_int_from_parser(GKeyFile *parser, const char *key, int *value);
void free_tz_entry(gpointer entry);
void read_tz_list(GKeyFile *parser);
// read a config file and update the settings hash. default any preferences not set
void read_config(void);

#endif
