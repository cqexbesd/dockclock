#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include <glib.h>
#include <glib-object.h>
#include <glib-unix.h>

#include <time.h>

#include "dockclock.h"
#include "prefs.h"

// update the labels to make sure they always reflect the current date/time
void update_display(struct widgets *widgets) {
	time_t current_time;
	struct tm *tm;
	char buffer[128]; // it has to fit into the dock so who wants a date longer than this?
	
	// get the current time
	current_time = time(NULL);
	tm = localtime(&current_time);

	// update time label
	strftime(buffer, sizeof(buffer), pref_for_key(PREFS_TIME_FORMAT), tm);
	gtk_label_set_text(widgets->time, buffer);
	// update date label
	strftime(buffer, sizeof(buffer), pref_for_key(PREFS_DATE_FORMAT), tm);
	gtk_label_set_text(widgets->date, buffer);
}

// call back function that runs every minute to keep the clock updated
gboolean schedule_display_updates(gpointer widgets) {
	time_t current_time;
	struct tm *tm;

	// do an initial update
	update_display((struct widgets *)widgets);
	
	// now we need to schedule ourselves to run again when the minute expires
	// (just leaving it at every 60 secs drifts eventually so we do a rough calculation)
	current_time = time(NULL);
	tm = localtime(&current_time);
	g_timeout_add_seconds(60 - tm->tm_sec, schedule_display_updates, widgets);

	return FALSE;
}

// apply the styles (font size, bold etc) to a label
void set_style_on_label(GtkLabel *label, const char *style_string) {
	PangoAttrList *attr_list;
	PangoAttribute *attr;
	PangoFontDescription *fd;

	// if there is a style specified then apply it
	if (style_string != NULL) {
		// string -> font_description
		fd = pango_font_description_from_string(style_string);
		// font_description -> attribute
		attr = pango_attr_font_desc_new(fd);
		// create an attribute list
		attr_list = pango_attr_list_new();
		// add our attribute
		pango_attr_list_insert(attr_list, attr);
		// apply list to widget
		gtk_label_set_attributes(label, attr_list);
		// clean up memory
		pango_attr_list_unref(attr_list); // should handle its attributes as well
		pango_font_description_free(fd);
	}
}

// update all the widgets to have the styles specified in the prefs
void apply_styles_to_gui(struct widgets *widgets) {
	const int *intval;

	set_style_on_label(widgets->time, pref_for_key(PREFS_TIME_STYLE));
	set_style_on_label(widgets->date, pref_for_key(PREFS_DATE_STYLE));
	
	intval = (const int *)pref_for_key(PREFS_PAD_LEFT);
	gtk_widget_set_margin_start(GTK_WIDGET(widgets->frame), *intval);
	intval = (const int *)pref_for_key(PREFS_PAD_RIGHT);
	gtk_widget_set_margin_end(GTK_WIDGET(widgets->frame), *intval);
}

// create a label to display the time or date
void create_label(GtkLabel **label) {

	*label = GTK_LABEL(gtk_label_new(NULL));
	gtk_label_set_justify(*label, GTK_JUSTIFY_CENTER);
	gtk_label_set_xalign(*label, 0.5);
	gtk_label_set_yalign(*label, 0.5);
	gtk_widget_set_margin_start(GTK_WIDGET(*label), 2);
	gtk_widget_set_margin_end(GTK_WIDGET(*label), 2);
}

// create all the GUI elements
void create_gui(struct widgets *widgets) {
	GtkWidget *grid;

	// first the main window
	widgets->window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));

	// set a title for screen readers etc to use
	gtk_window_set_title(widgets->window, "Clock");
	//gtk_window_set_default_size(widgets->window, 1, 1);

	// a frame to divide off the clock from the rest of the dock
	widgets->frame = GTK_FRAME(gtk_frame_new(NULL));
	gtk_container_add(GTK_CONTAINER(widgets->window), GTK_WIDGET(widgets->frame));

	// a grid so we can have multiple labels
	grid = gtk_grid_new();
	g_object_set(grid, "orientation", GTK_ORIENTATION_VERTICAL, NULL);
	gtk_container_add(GTK_CONTAINER(widgets->frame), grid);

	// create the label for the time
	create_label(&(widgets->time));
	gtk_container_add(GTK_CONTAINER(grid), GTK_WIDGET(widgets->time));

	// create the label for the date
	create_label(&(widgets->date));
	gtk_container_add(GTK_CONTAINER(grid), GTK_WIDGET(widgets->date));

	// apply the preferences to the widgets
	apply_styles_to_gui(widgets);
	// give the labels some data before they are shown
	update_display(widgets);

	// now show everything
	gtk_widget_show_all(GTK_WIDGET(widgets->window));

	{
		Display *display;
		Window window;
		XWMHints hints;

		display = GDK_WINDOW_XDISPLAY(gtk_widget_get_window(GTK_WIDGET(widgets->window)));
		window  = GDK_WINDOW_XID(gtk_widget_get_window(GTK_WIDGET(widgets->window)));
		hints.initial_state = WithdrawnState; 
		hints.flags=StateHint;
		XSetWMHints(display, window, &hints); 
	}
}

// called on sighup to reread the config file and make the GUI match
gboolean sighup_handler(gpointer widgets) {

	// get any changed settings
	read_config();
	// update any new styles
	apply_styles_to_gui(widgets);
	// redisplay the data in case the format has changed
	update_display((struct widgets *)widgets);
	// force the window to take its natural size (it must be (1, 1) or bigger)
	gtk_window_resize(((struct widgets *)widgets)->window, 1, 1);

	return TRUE;
}

const char *time_string_for_timezone(GDateTime *now, GTimeZone *tz, const char *format) {
	GDateTime *adjusted;
	const char *rv;

	adjusted = g_date_time_to_timezone(now, tz);
	rv = g_date_time_format(adjusted, format);
	g_date_time_unref(adjusted);

	return rv;
}

void build_tz_gui(GtkWindow **window, GSList *tzs) {
	GtkGrid *grid;
	GtkLabel *label, *last_label, *time_widget;
	GDateTime *now;
	const char *time_as_string;

	// create the new window
	*window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_POPUP));
	// add a grid to lay out our labels in
	grid = GTK_GRID(gtk_grid_new());
	//gtk_grid_set_column_homogeneous(grid, TRUE);
	gtk_grid_set_column_spacing(grid, 4);
	gtk_container_add(GTK_CONTAINER(*window), GTK_WIDGET(grid));
	// now find the current time
	now = g_date_time_new_now_utc();
	// now add each label in turn
	for (last_label = NULL; tzs != NULL; last_label = label, tzs = g_slist_next(tzs)) {
		label = GTK_LABEL(gtk_label_new(((struct time_zone *)(tzs->data))->label));
		gtk_label_set_xalign(label, 1);
		gtk_label_set_yalign(label, 0.5);
		gtk_label_set_justify(label, GTK_JUSTIFY_RIGHT);
		gtk_widget_set_margin_start(GTK_WIDGET(label), 2);
		gtk_widget_set_margin_end(GTK_WIDGET(label), 3);
		set_style_on_label(label, pref_for_key(PREFS_TZ_LABEL_STYLE));
		gtk_grid_attach_next_to(grid, GTK_WIDGET(label), GTK_WIDGET(last_label), GTK_POS_BOTTOM, 1, 1);

		time_as_string = time_string_for_timezone(now, ((struct time_zone *)(tzs->data))->tz, pref_for_key(PREFS_TZ_TIME_FORMAT));
		time_widget = GTK_LABEL(gtk_label_new(time_as_string));
		g_free((gpointer)time_as_string);
		gtk_label_set_xalign(time_widget, 0);
		gtk_label_set_yalign(time_widget, 0.5);
		gtk_label_set_justify(time_widget, GTK_JUSTIFY_LEFT);
		gtk_widget_set_margin_end(GTK_WIDGET(time_widget), 2);
		set_style_on_label(time_widget, pref_for_key(PREFS_TZ_TIME_STYLE));
		gtk_grid_attach_next_to(grid, GTK_WIDGET(time_widget), GTK_WIDGET(label), GTK_POS_RIGHT, 1, 1);
	}
	// free our time stamp
	g_date_time_unref(now);
	// position it
	gtk_window_set_position(*window, GTK_WIN_POS_MOUSE);
	// display it
	gtk_widget_show_all(GTK_WIDGET(*window));
}

gboolean on_click(GtkWidget *widget, GdkEvent *event, gpointer tz_window) {
	GSList *tzs;

	if ((event->type == GDK_BUTTON_PRESS) &&
			(((GdkEventButton *)event)->button == 1)) {
		// show popup window
		tzs = (GSList *)pref_for_key(PREFS_TZ_LIST);
		if (tzs != NULL) {
			build_tz_gui((GtkWindow **)tz_window, tzs);
		} else {
			// nothing to do
			*((GtkWindow **)tz_window) = NULL;
		}
	} else if ((event->type == GDK_BUTTON_RELEASE) &&
			(((GdkEventButton *)event)->button == 1)) {
		// destroy
		if (*((GtkWindow **)tz_window) != NULL) {
			gtk_widget_destroy(GTK_WIDGET(*((GtkWindow **)tz_window)));
			// we sometimes can get mouse up with no intervening mouse down so
			// make sure we don't destroy an already destroyed window
			*((GtkWindow **)tz_window) = NULL;
		}
	}

	return TRUE;
}

int main(int argc, char *argv[]) {
	GError *gerr = NULL;
	GOptionContext *goption_context;
	const char *conf_file_path = NULL;
	struct widgets widgets; // somewhere to store the things that need updating
	GtkWindow *tz_window;

	// handle our args
	GOptionEntry cli_opts[] = {
		{ "conf", 'c', 0, G_OPTION_ARG_FILENAME, &conf_file_path, "Path to configuration file", "file" },
		{ NULL }
	};

	goption_context = g_option_context_new(" - display a clock in the dock");
	g_option_context_add_main_entries(goption_context, cli_opts, NULL);
	g_option_context_add_group(goption_context, gtk_get_option_group(TRUE));
	if (! g_option_context_parse(goption_context, &argc, &argv, &gerr)) {
		g_error("parse of command line options failed: %s", gerr->message);
		g_clear_error(&gerr);
	}

	// get the prefs system ready and load defaults
	initialise_prefs(conf_file_path);

	// finished with the options now we have saved any path
	g_option_context_free(goption_context);
	if (conf_file_path != NULL) {
		g_free((gpointer)conf_file_path);
	}

	// read any config file (maybe the default location)
	read_config();

	// create the GUI stuff
	create_gui(&widgets);

	// make sure we update the clock every minute
	schedule_display_updates((gpointer)&widgets);

	// install our handler for SIGHUP to reload the settings
	g_unix_signal_add(SIGHUP, sighup_handler, &widgets);

	// install our handler for mouse click to show other time zones
	tz_window = NULL; // in case we get a mouse up before a mouse down
	gtk_widget_add_events(GTK_WIDGET(widgets.window), GDK_BUTTON_PRESS_MASK);
	gtk_widget_add_events(GTK_WIDGET(widgets.window), GDK_BUTTON_RELEASE_MASK);
	g_signal_connect(widgets.window, "button-press-event", G_CALLBACK(on_click), (gpointer)&tz_window);
	g_signal_connect(widgets.window, "button-release-event", G_CALLBACK(on_click), (gpointer)&tz_window);
	
	// and finally hand control over to the GTK
	gtk_main();

	return 0;
}
